# To Do List Frontend

_InfoCasas Full-Stack Challenge Criteria_


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Requerimientos 📋
- Npm (`https://docs.npmjs.com/downloading-and-installing-node-js-and-npm`)
- NodeJS (Versión utilizada `https://nodejs.org/download/release/v12.20.2/`)
- React (`https://es.reactjs.org/docs`)
- Git (`https://git-scm.com/downloads`)

### Instalación 🔧

Nos ubicamos en cualquier zona en la que desee colocar el proyecto desde la terminal. Clonar el repositorio usando el siguiente comando.

```
git clone https://gitlab.com/alejandrojsrv/to-do-app
```


## Ejecutamos npm (Node package manager)

Estando en la misma ubicación, ejecutamos lo siguiente:

```
npm install
```

## Despliegue 📦
Para ejecutar la aplicación en modo de desarrollo ejecutamos:
```
npm run start
```
Abrimos `http://localhost:3000` para verlo en el navegador.

## Construido con 🛠️
* [React JS](`https://es.reactjs.org/docs`) - El framework del frontend usado 

## Autor ✒️

* **Alejandro Sojo** 