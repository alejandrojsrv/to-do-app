
import API from "../plugins/axios";
import { useState, useEffect } from "react";
import ItemTask from "../components/ItemTask";
import moment from "moment";
import { plusIcon } from "../style/icons";
import ItemTaskForm from "../components/ItemTaskForm";


const Tasks = () => {

    const [tasks, setTasks] = useState([]);

    const [showForm, setShowForm] = useState(false)

    const [params, setParams] = useState({
        name: '',
        completed: 'all',
    })

    const onChangeInput = (e) => {

        const name = e.target.name;
        const value = e.target.value;

        setParams({
            ...params,
            [name]: value
        })
    }

    const noTask = () => {
        return (<p> No tasks to show </p>)
    }

    const getTask = () => {

        let query = "?"

        Object.keys(params).forEach(key => {
            if (!["all", "", null].includes(params[key])) {
                query += query !== '?' ? `${key}=${params[key]}` : `&${key}=${params[key]}`;
            }
        });

        API.get(`/tasks${query}`).then(rs => {
            setTasks(rs.data)
        });
    }

    const onCreatedTask = () => {
        setShowForm(false)
        setParams({})
        getTask();
    }


    useEffect(() => getTask(), []);
    useEffect(() => getTask(), [params]);

    return (
        <div className="container">
            <div className="header-tasks">
                <div className="title">
                    <h1>  To do list  </h1>
                    <h4>  {moment(new Date()).format("dddd,  DD MMMM")} </h4>
                </div>

                <div className="options-header">
                    <span className={params.completed === 'all' ? "active" : ""} onClick={() => setParams({ ...params, completed: 'all' })}> All </span>
                    <span className={params.completed === 1 ? "active" : ""} onClick={() => setParams({ ...params, completed: 1 })}> Completed </span>
                </div>

            </div>

            <div className="search-tasks">
                <input type="text" value={params.name} name="name" placeholder="Search task by name" onChange={(e) => onChangeInput(e)} />
            </div>

            <div className="create-task">
                {
                    showForm ?
                        <ItemTaskForm onCreatedTask={onCreatedTask}></ItemTaskForm> :
                        <div className="item-task no-form" onClick={() => setShowForm(true)}>
                            {plusIcon}
                        </div>
                }

            </div>

            <div className="list-tasks">
                {
                    tasks.length > 0 ? tasks.map(item => (<ItemTask key={item.id} item={item} onUpdatedTask={getTask} />)) : noTask()
                }

            </div>
        </div>)

}
export default Tasks;