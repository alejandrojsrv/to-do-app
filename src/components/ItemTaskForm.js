import API from "../plugins/axios";
import { useState } from "react";

const ItemTaskForm = (props) => {

    const initialState = {
        name: "",
        completed: 0,
        created_at: '',
        update_at: ''
    }

    const [task, setTask] = useState(initialState)

    const onChangeInput = (e) => {

        const name = e.target.name;
        const value = e.target.value;

        setTask({
            ...task,
            [name]: value
        })
    }

    const saveTask = (e) => {
        e.preventDefault();

        API.post('/tasks', task).then(rs => {

            if (props.onCreatedTask) {
                props.onCreatedTask();
            }

            setTask(initialState)
        })
    }

    return (
        <form onSubmit={saveTask} className="item-task">
            <input type="text" value={task.name} onChange={onChangeInput} name="name" placeholder="Insert the name of task" style={{ width: '100%' }} />
            <div className="options-task">
                <button type="submit"> Save </button>
            </div>
        </form>
    )
}

export default ItemTaskForm;