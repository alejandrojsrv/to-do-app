import Nav from "./components/Nav";
import Tasks from "./views/Tasks";
import './style/App.css'


const App = () => {
  return (
    <div className="app">
      <Nav />
      <Tasks />
    </div>
  );
}

export default App;
