

import moment from 'moment'
import { useState, useEffect } from 'react'
import API from '../plugins/axios'
import { checkSvg, circleSvg, editSvg, saveSvg, sendingIcon, trashSvg } from '../style/icons'

const ItemTask = (props) => {

    const [task, setTask] = useState(props.item)

    const [options, setOptions] = useState({
        isEditing: false,
        isSending: false
    })

    const statusIcon = task.completed ? checkSvg : circleSvg

    const onChangeInput = (e) => {

        const name = e.target.name;
        const value = e.target.value;

        setTask({
            ...task,
            [name]: value
        })

    }


    const updateTask = async () => {
        try {

            await API.put(`/tasks/${task.id}`, task)

            setOptions({
                isEditing: false,
                isSending: false
            })

            props.onUpdatedTask()

        } catch (error) {
            console.log(error)
        }
    }

    const completeTask = async () => {

        setOptions({ ...options, isSending: true })

        let taskUpdate = { ...task, completed: !task.completed }

        await API.put(`/tasks/${task.id}`, taskUpdate)

        setOptions({ ...options, isSending: false })

        props.onUpdatedTask();
    }

    const deleteTask = async () => {
        try {
            const rs = await API.delete(`/tasks/${task.id}`)

            if (rs.data.delete) {
                props.onUpdatedTask()
            }
        } catch (error) {
            console.log(error)
        }

    }

    useEffect(() => {
        setTask(props.item)
    }, [props.item]);


    return (
        <div className="item-task">

            <div className="icon-task" onClick={() => completeTask()}> {!options.isSending ? statusIcon : sendingIcon} </div>

            <div className="content-task">
                <p className={`title-task ${task.completed ? 'completed' : ''}`}>
                    {
                        options.isEditing ? <input type="text" name="name" value={task.name} onChange={onChangeInput} style={{ margin: '0' }} autoFocus /> : task.name
                    }
                </p>
                <p className="date-task"> {`${moment(task.created_at).format('DD MMM YY ')} at ${moment(task.created_at).format('h:mm ')}`} </p>
            </div>

            <div className="options-task">
                {
                    options.isEditing ?
                        <>
                            <div className="icon-task" onClick={() => updateTask()}> {saveSvg} </div>
                        </> :
                        <>
                            <div className="icon-task" onClick={() => setOptions({ ...options, isEditing: true })}> {editSvg} </div>
                            <div className="icon-task" onClick={() => deleteTask()}> {trashSvg} </div>
                        </>
                }
            </div>

        </div>
    )
}

export default ItemTask;